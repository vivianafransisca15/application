package com.example.catalogmovie.core;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Upsert;

import com.example.catalogmovie.model.Todo;

import java.util.List;

@Dao
public interface UserDao {
    @Query("SELECT * FROM Todo")
    List<Todo> getAll();

    @Upsert // upload data from API and insert it to database
    void insertAll(List<Todo> todos);

    @Delete
    void delete(Todo todo);
}
