package com.example.catalogmovie.modul.apidatabase;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.catalogmovie.R;
import com.example.catalogmovie.adapter.MovieAdapter;
import com.example.catalogmovie.core.CallBackDeleteItem;
import com.example.catalogmovie.core.Injection;
import com.example.catalogmovie.model.Todo;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainViewContract {

    private MovieAdapter adapter;
    private ProgressBar loader;

    RecyclerView recyclerView;
    Button downloadButton;
    Button databaseButton;
    Button filterButton;
    private MainPresenterContract presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inisialisasi ProgressBar
        loader = findViewById(R.id.loader);

        downloadButton = findViewById(R.id.download_button);
        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Show loader
                loader.setVisibility(View.VISIBLE);
                presenter.doGetTodoList();
            }
        });

        databaseButton = findViewById(R.id.database_button);
        databaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Show loader
                loader.setVisibility(View.VISIBLE);
                presenter.doGetDatabaseList();
            }
        });

        filterButton = findViewById(R.id.filter);
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loader.setVisibility(View.VISIBLE);
                presenter.doGetFilter();
            }
        });

        recyclerView = findViewById(R.id.rvMovie);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new MovieAdapter(MainActivity.this, new ArrayList<>(), new CallBackDeleteItem() {
            @Override
            public void doDelete(Todo todo, int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Apakah Anda yakin ingin menghapus item ini?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                presenter.doGetDeleteOneItem(todo);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Toast.makeText(MainActivity.this, "Penghapusan dibatalkan", Toast.LENGTH_SHORT).show();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        recyclerView.setAdapter(adapter);
        new MainPresenter(this, Injection.provideMainRepo(MainActivity.this));
    }

    @Override
    public void setPresenter(MainPresenterContract presenter) {
        this.presenter = presenter;
    }

    @Override
    public void implementTodoList() {
        // Hide loader when task is finished
        loader.setVisibility(View.GONE);
        Toast.makeText(this, "Berhasil Get Api Todo", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void implementTodoSave() {
        // Hide loader when task is finished
        loader.setVisibility(View.GONE);
    }

    @Override
    public void implementToDoList(List<Todo> object) {
        // Hide loader when task is finished
        loader.setVisibility(View.GONE);
        adapter.refreshData(object);
    }

    @Override
    public void implementToDoDelete(Todo todo) {
        loader.setVisibility(View.GONE);
        adapter.deleteData(todo);

    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDeleteErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        super.onPointerCaptureChanged(hasCapture);
    }
}
