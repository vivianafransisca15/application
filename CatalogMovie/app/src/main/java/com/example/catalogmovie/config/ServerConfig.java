package com.example.catalogmovie.config;

public class ServerConfig {
    public static final String URL_BASE = "https://jsonplaceholder.typicode.com/";
    public static final String API_ENDPOINT = URL_BASE;
}
