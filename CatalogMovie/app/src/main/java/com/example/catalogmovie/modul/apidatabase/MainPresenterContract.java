package com.example.catalogmovie.modul.apidatabase;

import com.example.catalogmovie.model.Todo;

public interface MainPresenterContract {
    void doGetTodoList();

    void doGetDatabaseList();
    void doGetFilter();

    void doGetDeleteOneItem(Todo todo);
}
