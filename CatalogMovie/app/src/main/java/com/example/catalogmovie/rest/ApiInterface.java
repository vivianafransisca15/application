package com.example.catalogmovie.rest;

import com.example.catalogmovie.model.Todo;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiInterface {
    @GET("todos")
    Call<List<Todo>> getMovie();
}
