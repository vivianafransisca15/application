package com.example.catalogmovie.core.apidatabase;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;

import androidx.core.os.HandlerCompat;
import androidx.room.Room;

import com.example.catalogmovie.adapter.MovieAdapter;
import com.example.catalogmovie.core.AppDatabase;
import com.example.catalogmovie.core.CallbackData;
import com.example.catalogmovie.model.Todo;
import com.example.catalogmovie.rest.ApiClient;
import com.example.catalogmovie.rest.ApiInterface;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainRepo implements MainRepoContract{
    private final AppDatabase db;
    private final Context context;

    private MovieAdapter adapter;

    public MainRepo(Context context) {
        this.context = context;
        db = Room.databaseBuilder(context,
                AppDatabase.class, "database-name").build();
    }

    @Override
    public void getToDoList(CallbackData callbackData) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Todo>> call = apiInterface.getMovie();
        call.enqueue(new Callback<List<Todo>>() {
            @Override
            public void onResponse(Call<List<Todo>> call, Response<List<Todo>> response) {
                callbackData.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<Todo>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void doSaveToDoList(List<Todo> object,CallbackData callbackData) {
        Executors.newCachedThreadPool().execute(() -> {
            try {
                db.userDao().insertAll(object);

                HandlerCompat.createAsync(Looper.getMainLooper()).post(() -> callbackData.onSuccess(new Object()));

            }catch (Exception e){
                HandlerCompat.createAsync(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        callbackData.onError(e);
                    }
                });

            }
        });

    }

    @Override
    public void getDatabaseList(CallbackData callbackData) {
        Executors.newCachedThreadPool().execute(() -> {
            try {
            List<Todo> todos = db.userDao().getAll();
            ((Activity)context).runOnUiThread(() -> {
                callbackData.onSuccess(todos);
            });
            } catch (Exception e) {
                callbackData.onError(e);
            }
        });
    }

    @Override
    public void getFilter(CallbackData callbackData) {
        Executors.newCachedThreadPool().execute(() -> {
            try {
                List<Todo> todos = db.userDao().getAll();

                if (todos.size() > 5) {
                    todos = todos.subList(0, 5);
                }

                List<Todo> finalTodos = todos;
                ((Activity) context).runOnUiThread(() -> {
                    callbackData.onSuccess(finalTodos);
                });
            } catch (Exception e) {
                callbackData.onError(e);
            }
        });
    }

    @Override
    public void getDeleteOneItem(Todo todo, CallbackData callbackData) {
        Executors.newCachedThreadPool().execute(() -> {
            try {
                db.userDao().delete(todo);

                HandlerCompat.createAsync(Looper.getMainLooper()).post(() -> callbackData.onSuccess(null));
            } catch (Exception e) {
                HandlerCompat.createAsync(Looper.getMainLooper()).post(() -> callbackData.onError(e));
            }
        });
    }

}
