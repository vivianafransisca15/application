package com.example.catalogmovie.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.catalogmovie.R;
import com.example.catalogmovie.core.CallBackDeleteItem;
import com.example.catalogmovie.model.Todo;

import java.util.ArrayList;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> {
    private Context context;
    private List<Todo> resultList;
//    private OnItemClickListener itemClickListener;
    private CallBackDeleteItem callBackDeleteItem;

    public MovieAdapter(Context context, List<Todo> resultList, CallBackDeleteItem callBackDeleteItem) {
        this.context = context;
        this.resultList = resultList;
        this.callBackDeleteItem = callBackDeleteItem;
    }

//    public void setOnItemClickListener(OnItemClickListener listener) {
//        this.itemClickListener = listener;
//    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Todo currentTodo = resultList.get(position);
        holder.tvTitle.setText(currentTodo.getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callBackDeleteItem.doDelete(currentTodo, position);
            }
        });
    }

    public void refreshData(List<Todo> resultList) {
        this.resultList.clear();
        this.resultList.addAll(resultList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return resultList.size();
    }

    public void deleteData(Todo todo) {
        resultList.remove(todo);
        notifyDataSetChanged();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
        }
    }
}

