package com.example.catalogmovie.core.apidatabase;

import android.telecom.Call;

import com.example.catalogmovie.core.CallbackData;
import com.example.catalogmovie.model.Todo;

import java.util.List;

public interface MainRepoContract {
    void getToDoList(CallbackData callbackData);

    void doSaveToDoList(List<Todo> object, CallbackData callbackData);

    void getDatabaseList(CallbackData callbackData);

    void getFilter(CallbackData callbackData);

    void getDeleteOneItem(Todo todo, CallbackData callbackData);
}
