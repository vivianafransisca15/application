package com.example.catalogmovie.core;

import com.example.catalogmovie.model.Todo;

public interface CallBackDeleteItem {
    void doDelete(Todo todo, int position);
}
