package com.example.catalogmovie.modul.apidatabase;

import com.example.catalogmovie.model.Todo;

import java.util.List;

public interface MainViewContract {
    void setPresenter(MainPresenterContract presenter);
    void implementTodoList();
    void implementTodoSave();

    void implementToDoList(List<Todo> object);
    void implementToDoDelete(Todo todo);

    void showErrorMessage(String s);

    void showDeleteErrorMessage(String message);
}
