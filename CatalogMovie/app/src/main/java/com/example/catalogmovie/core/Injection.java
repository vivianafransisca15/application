package com.example.catalogmovie.core;

import android.content.Context;

import com.example.catalogmovie.core.apidatabase.MainRepo;
import com.example.catalogmovie.core.apidatabase.MainRepoContract;
//import com.example.catalogmovie.modul.apidatabase.MainActivity;

public class Injection {
    public static MainRepoContract provideMainRepo(Context context) {
        MainRepo mainRepo = new MainRepo(context);
        return mainRepo;
    }
}
