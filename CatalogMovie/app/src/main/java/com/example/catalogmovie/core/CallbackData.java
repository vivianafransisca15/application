package com.example.catalogmovie.core;

import com.example.catalogmovie.model.Todo;

public interface CallbackData {
    void onSuccess(Object object);
    void onError(Throwable throwable);
}
