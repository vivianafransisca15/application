package com.example.catalogmovie.modul.apidatabase;

import android.content.Context;
import android.widget.Toast;

import com.example.catalogmovie.core.CallbackData;
import com.example.catalogmovie.core.apidatabase.MainRepoContract;
import com.example.catalogmovie.model.Todo;

import java.util.List;

public class MainPresenter implements MainPresenterContract {
    private final MainViewContract mainViewContract;
    private final MainRepoContract mainRepoContract;

    public MainPresenter(MainViewContract mainViewContract, MainRepoContract mainRepoContract) {
        this.mainViewContract = mainViewContract;
        this.mainRepoContract = mainRepoContract;
        mainViewContract.setPresenter(this);
    }

    @Override
    public void doGetTodoList() {
        mainRepoContract.getToDoList(new CallbackData() {
            @Override
            public void onSuccess(Object object) {
                saveTodo(object);
            }

            @Override
            public void onError(Throwable throwable) {
                mainViewContract.showErrorMessage("Error occurred: " + throwable.getMessage());
            }
        });
    }

    @Override
    public void doGetDatabaseList() {
        mainRepoContract.getDatabaseList(new CallbackData() {
            @Override
            public void onSuccess(Object object) {
               mainViewContract.implementToDoList((List<Todo>) object);
            }

            @Override
            public void onError(Throwable throwable) {
                mainViewContract.showErrorMessage("Error occurred: " + throwable.getMessage());
            }
        });
    }

    @Override
    public void doGetFilter() {
        mainRepoContract.getFilter(new CallbackData() {
            @Override
            public void onSuccess(Object object) {
                mainViewContract.implementToDoList((List<Todo>) object);
            }

            @Override
            public void onError(Throwable throwable) {
                mainViewContract.showErrorMessage("Error occurred: " + throwable.getMessage());
            }
        });
    }

    @Override
    public void doGetDeleteOneItem(Todo todo) {
        mainRepoContract.getDeleteOneItem(todo, new CallbackData() {
            @Override
            public void onSuccess(Object object) {
                mainViewContract.implementToDoDelete(todo);
            }

            @Override
            public void onError(Throwable throwable) {
                mainViewContract.showErrorMessage("Error occurred while deleting item: " + throwable.getMessage());
            }
        });
    }

    private void saveTodo(Object object) {
        mainRepoContract.doSaveToDoList((List<Todo>) object, new CallbackData() {
            @Override
            public void onSuccess(Object object) {
                mainViewContract.implementTodoList();
            }

            @Override
            public void onError(Throwable throwable) {
                mainViewContract.showErrorMessage("Error occurred: " + throwable.getMessage());
            }
        });
    }
}
